package com.example.ymsk.debugtaskapp.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Itemのまとまり(リスト)を保持するクラス
 */
public class ItemRetriever implements Serializable {
    private List<Item> mItems;
    private List<Item> mSelectedItems;
    private int mCharge = 0;

    public static String getFormattedPrice(int price){
        return "¥ " + price;
    }

    public enum Order {
        All,
        Selected
    }

    public ItemRetriever() {
        super();
    }

    public ItemRetriever(List<Item> itemList) {
        this();
        mItems = itemList;
        mSelectedItems = new ArrayList<>();
    }

    /**
     * リストポジションからアイテムを取得する
     */
    public Item getItem(int position){
        return mItems.get(position);
    }

    /**
     * 選択中のアイテム数を返す
     */
    public int getSelectedItemCount(){
        return mSelectedItems.size();
    }

    /**
     * 合計金額を返す
     */
    public int getCharge(){
        return mCharge;
    }

    /**
     * 選択状態を反転する
     * 選択状態変化後の合計金額を計算する
     */
    public void updateSelectedState(int position) {
        Item item = getItem(position);
        final boolean CHANGED_STATE = !item.isSelected();
        final  int price = item.getPrice();

        item.setSelected(CHANGED_STATE);

        if(CHANGED_STATE) {
            mSelectedItems.add(item);
        }else {
            mSelectedItems.remove(item);
        }
        mCharge += CHANGED_STATE ? price : -price;/* 三項演算子 */
    }

    /**
     * オーダーされたアイテムリストを取得する
     */
    public List<Item> getItems(Order order){
        List<Item> resultList = new ArrayList<>();

        switch (order){
            case All:
                resultList = mItems;
                break;
            case Selected:
                resultList = mSelectedItems;
                break;
            default:
                break;
        }
        return resultList;
    }

    /**
     * 商品のクラス
     *
     * getterはpublicとするが
     * setterはprivateとし、Retriever経由でしかアクセスさせない
     *
     * -> インナークラスは必ずpublic staticにする。
     */
    public static class Item implements Serializable{

        public Item(String name, int price, int imageResId){
            mName = name;
            mPrice = price;
            mImageResId = imageResId;
        }

        public String getName(){
            return mName;
        }
        public int getPrice(){
            return mPrice;
        }
        public int getImageResId(){
            return mImageResId;
        }
        public boolean isSelected(){
            return mIsSelected;
        }

        private void setSelected(boolean isSelected){
            mIsSelected = isSelected;
        }

        private final String mName;         // 商品名
        private final int mPrice;           // 値段
        private final int mImageResId;     // 画像リソースのID
        private boolean mIsSelected;        // 選択状態
    }
}