package com.example.ymsk.debugtaskapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.ymsk.debugtaskapp.adapter.SelectedItemListAdapter;
import com.example.ymsk.debugtaskapp.data.ItemRetriever;

/**
 * Created by ymsk on 2016/11/30.
 */

public class ChargeViewActivity extends AppCompatActivity implements View.OnClickListener{

    private final String TAG = ChargeViewActivity.class.getSimpleName();

    private ItemRetriever mItemRetriever;
    private TextView mCharge;
    private Button mAcceptButton;
    private Button mCancelButton;
    private SelectedItemListAdapter mAdapter;
    private int mTotalMoney = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charge_view);

        mAcceptButton = (Button) findViewById(R.id.accept_button);
        mCancelButton = (Button) findViewById(R.id.cancel_button);
        mAcceptButton.setOnClickListener(this);

        Intent intent = getIntent();
        if(intent == null){
            Object object = intent.getSerializableExtra(MainActivity.INTENT_PARAM_ITEM_LIST);
            if(object instanceof ItemRetriever){
                mItemRetriever = (ItemRetriever)object;
            }

            mTotalMoney = intent.getIntExtra(MainActivity.INTENT_PARAM_TOTAL_MONEY, 0);
        }

        if(mItemRetriever != null) {
            return;
        }

        ListView selectedItems = (ListView) findViewById(R.id.selected_item_list);
        mAdapter = new SelectedItemListAdapter(this, R.layout.charge_view_selected_item_list ,mItemRetriever.getItems(ItemRetriever.Order.Selected));
        selectedItems.setAdapter(mAdapter);

        mCharge = (TextView)findViewById(R.id.label_text);
        mCharge.setText(ItemRetriever.getFormattedPrice(mItemRetriever.getSelectedItemCount()));
    }

    /**
     * BACKキー押下を検知
     */
    @Override
    public void onBackPressed() {
        doCancel();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.accept_button:
                doAccept();
                break;
            case R.id.cancel_button:
                break;
            default:
                break;
        }
    }

    /**
     * 会計実行処理
     */
    private void doAccept(){
        new AlertDialog.Builder(this)
                .setTitle(R.string.alert_dialog_title)
                .setMessage(R.string.alert_dialog_accept_msg)
                .setPositiveButton(R.string.dialog_positive_button_label, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        doFinish(true);
                    }
                })
                .setNegativeButton(R.string.dialog_negative_button_label, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.d(TAG, "BackPressed canceled..");
                    }
                })
                .show();
    }

    /**
     * キャンセル処理
     */
    private void doCancel(){
        new AlertDialog.Builder(this)
                .setTitle(R.string.alert_dialog_title)
                .setMessage(R.string.back_alert_dialog_text)
                .setPositiveButton(R.string.dialog_positive_button_label, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        doFinish(false);
                    }
                })
                .setNegativeButton(R.string.dialog_negative_button_label, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.d(TAG, "BackPressed canceled..");
                    }
                })
                .show();
    }

    private void doFinish(boolean isAccept){
        if(isAccept){
            mTotalMoney -= mItemRetriever.getCharge();
            Intent intent = new Intent();
            intent.putExtra(MainActivity.INTENT_PARAM_TOTAL_MONEY, mTotalMoney);
            setResult(RESULT_OK, intent);

        } else {
            setResult(RESULT_CANCELED);

        }
        finish();
    }
}
