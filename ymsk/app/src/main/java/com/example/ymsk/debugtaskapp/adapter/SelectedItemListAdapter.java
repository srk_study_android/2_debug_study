package com.example.ymsk.debugtaskapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

import com.example.ymsk.debugtaskapp.data.ItemRetriever;

import java.util.List;

/**
 * Created by ymsk on 2016/12/04.
 */

public class SelectedItemListAdapter extends ItemListAdapter {

    public SelectedItemListAdapter(Context context, int resource, List<ItemRetriever.Item> items) {
        super(context, resource, items);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return super.getView(position, convertView, parent);
    }
}
