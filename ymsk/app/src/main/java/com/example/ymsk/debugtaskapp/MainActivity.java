package com.example.ymsk.debugtaskapp;

import android.content.Intent;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ymsk.debugtaskapp.adapter.ItemListAdapter;
import com.example.ymsk.debugtaskapp.data.ItemRetriever;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements
        AdapterView.OnItemClickListener {

    public static final String INTENT_PARAM_ITEM_LIST = "intent_param_item_list";
    public static final String INTENT_PARAM_TOTAL_MONEY = "intent_param_total_money";
    public static final int REQUEST_CODE_MONEY_IN_HAND = 100;

    // クラス名を保持。ログ出力などに使う
    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int KEY_LIST_ITEM_BIG_MAC = 0;
    private static final int KEY_LIST_ITEM_TERIYAKI = 1;
    private static final int KEY_LIST_ITEM_DOUBLE_CHEESE = 2;

    private final int INIT_MONEY_IN_HAND = 1000;

    private ItemRetriever mItemRetriever;
    private ItemListAdapter mItemListAdapter;
    private TextView mTotalMoneyTextView;
    private int mTotalMoney = INIT_MONEY_IN_HAND;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button checkButton = (Button)findViewById(R.id.check_button);
        checkButton.setOnClickListener(this);

        mTotalMoneyTextView = (TextView) findViewById(R.id.total_money);
        updateTotalMoney(true);
        initDataStub();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // onDestroy()でActivityは終了するが、メンバ変数はOnMemoryされたまま
        // 明示的にnullを参照させることでメモリ解放している
        mItemRetriever = null;
        mItemListAdapter = null;
    }

    /**
     * リストアイテムのクリックリスナー
     * @param adapterView adapterがセットされたビュー、つまりListView本体
     * @param view ListView中の1アイテム(子ビュー)
     * @param position 選択されたポジション(0開始)
     * @param id 選択されたアイテムのID
     */
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

        setItemSelected(view, position);

        switch (position) {
            case KEY_LIST_ITEM_BIG_MAC:
                Log.d(TAG, "ビッグマック、one please!");
                break;
            case KEY_LIST_ITEM_TERIYAKI:
                Log.d(TAG, "てりやきマック one please!");
                break;
            case KEY_LIST_ITEM_DOUBLE_CHEESE:
                Log.d(TAG, "ダブルチーズバーガー one please!");
                break;
            default:
                break;
        }
    }

    /**
     * Viewのクリックリスナー
     *
     * ListViewのクリックイベントはItemListAdapterが管理しているため、OnItemClick()で拾っている
     * 一般のView(今回だとCheckButton)のクリックイベントはここで拾う
     *
     * @param view
     */
    @Override
    public void onClick(View view) {
        if (view.getId() != R.id.check_button) {
            // CheckButton以外のイベントは不要
            return;
        }
        if (mItemRetriever.getSelectedItemCount() == 0) {
            Toast.makeText(this, "商品が選択されていません！", Toast.LENGTH_SHORT).show();
            return;
        }

        int charged = mTotalMoney - mItemRetriever.getCharge();
        if(charged < 0){
            Toast.makeText(this, "所持金が " + -charged + " 円たりません！", Toast.LENGTH_SHORT).show();
            return;
        }
        startNewActivity();
    }

    /**
     * ActionBarにOptionsMenuを表示させる
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item, menu);
        return true;
    }

    /**
     * OptionsMenuのクリックリスナー
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.reset_button:
                Toast.makeText(this, "リセットします!").show();
                updateTotalMoney(true);
                initDataStub();
                break;
            default:
                break;
        }
        return true;
    }

    /**
     * startActivityResult()で別Activityへ遷移した場合、
     * 遷移先Activityで処理した結果を遷移元(つまりこのクラス)のこのコールバックで受け取る
     *
     * @param requestCode 要求コード
     * @param resultCode 結果コード
     * @param data 受け取るデータ
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case REQUEST_CODE_MONEY_IN_HAND:
                if(resultCode == RESULT_OK) {
                    // 支払い結果を反映
                    Bundle bundle = data.getExtras();
                    mTotalMoney = bundle.getInt(INTENT_PARAM_TOTAL_MONEY);
                    updateTotalMoney(false);
                    initDataStub();
                } else if (resultCode == RESULT_CANCELED) {
                    initDataStub();
                    Log.d(TAG, "charge canceled..");
                }
                break;
            default:
                break;
        }
    }

    private void startNewActivity(){
        // PhotoViewActivityへ遷移
        Intent intent = new Intent(this, ChargeViewActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(INTENT_PARAM_ITEM_LIST, mItemRetriever);
        intent.putExtra(INTENT_PARAM_TOTAL_MONEY, mTotalMoney);
        Log.d(TAG, "charge=" + mItemRetriever.getCharge() + " / count=" + mItemRetriever.getItem(INIT_MONEY_IN_HAND));

        startActivityForResult(intent, REQUEST_CODE_MONEY_IN_HAND);
    }

    private void setItemSelected(View view, int position) {
        if(view == null || mItemListAdapter == null) {
            Log.d(TAG, "ERROR! Required objects are NULL...");
            return;
        }
        if(position >= mItemListAdapter.getCount()) {
            Log.d(TAG, "ERROR! this position is contents size over...");
            return;
        }

        mItemRetriever.updateSelectedState(position);
        final boolean UPDATED_ITEM_SELECTED_STATE = mItemRetriever.getItem(position).isSelected();

        if(UPDATED_ITEM_SELECTED_STATE) {
            view.setBackgroundColor(getColor(R.color.color_selected_bg));
        } else {
            view.setBackgroundColor(getColor(R.color.color_not_selected_bg));
        }
    }

    /**
     * List表示を確認するためのスタブ
     *
     * @return
     */
    private void initDataStub() {
        List<ItemRetriever.Item> items = new ArrayList<>();

        // 拡張性のない安易な実装
        // だから「スタブ」。本来ならばDBに商品データを登録しておき、そこから取得する

        items.add(KEY_LIST_ITEM_BIG_MAC, new ItemRetriever.Item("ビックマック", 370, R.drawable.ic_star_black_24dp));
        items.add(KEY_LIST_ITEM_TERIYAKI, new ItemRetriever.Item("てりやきマック", 310, R.drawable.teriyaki_mac));
        items.add(KEY_LIST_ITEM_DOUBLE_CHEESE, new ItemRetriever.Item("ダブルチーズバーガー", 340, R.drawable.dabuchi));

        mItemRetriever = new ItemRetriever(items);

        ListView listView = (ListView)findViewById(R.id.item_list_view);
        mItemListAdapter = new ItemListAdapter(this, R.layout.view_list_item, mItemRetriever.getItems(ItemRetriever.Order.ONLY_SET_MENU));
        listView.setAdapter(mItemListAdapter);
        listView.setOnItemClickListener(this);
    }

    private void updateTotalMoney(boolean init){
        if(init){
            mTotalMoney = INIT_MONEY_IN_HAND;
        }
        mTotalMoneyTextView.setText(ItemRetriever.getFormattedPrice(mTotalMoney));
    }
}
