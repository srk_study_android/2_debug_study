package com.example.ymsk.debugtaskapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ymsk.debugtaskapp.data.ItemRetriever;
import com.example.ymsk.debugtaskapp.R;

import java.util.List;

/**
 * Created by ymsk on 2016/11/30.
 *
 * Adapterとは、関連性のないクラス同士を繋げるクラス（GoF Javaデザインパターンの一つ）
 * UI応答を受け持つActivityクラスに、画面構成機能を受け持つViewクラスを接続する
 *
 * ArrayAdapterはリスト表示をサポートする
 *
 */
public class ItemListAdapter extends ArrayAdapter {

    private LayoutInflater mLayoutInflater;
    private int mResourceId;
    private List<ItemRetriever.Item> mItems;
    private Context mContext;

    public ItemListAdapter(Context context, int resource, List<ItemRetriever.Item> items) {
        super(context, resource, items);
        mResourceId = resource;
        mItems = items;
        mLayoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView != null) {
            view = convertView;
        } else {
            view = mLayoutInflater.inflate(mResourceId, null);
        }

        ItemRetriever.Item item = mItems.get(position);

        ((ImageView) view.findViewById(R.id.item_image)).setImageDrawable(mContext.getDrawable(item.getImageResId()));
        ((TextView) view.findViewById(R.id.item_name)).setText(item.getName());
        ((TextView) view.findViewById(R.id.item_price)).setText(ItemRetriever.getFormattedPrice(item.getPrice()));

        return view;
    }
}
